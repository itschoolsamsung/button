package com.example.al.a02_05__button;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
    Button btn;
    EditText edit;
    TextView txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn = (Button)findViewById(R.id.button);
        edit = (EditText)findViewById(R.id.editText);
        txt = (TextView)findViewById(R.id.text);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt.setText( edit.getText().toString() );
                Toast.makeText(getApplicationContext(), R.string.text, Toast.LENGTH_SHORT).show();
            }
        };

        btn.setOnClickListener(listener);

//        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                txt.setText( edit.getText().toString() );
//                Toast.makeText(getApplicationContext(), R.string.text, Toast.LENGTH_SHORT).show();
//            }
//        });
    }
}
